# **Phasmatrix**
*by docfo4r*



### **Basic functionality**
Phasmatrix provides an on-screen GUI during Phasmophobia gaming sessions.
This on-screen GUI provides tasks and informations to get used for second-tier evidence ghost identification.



### **Installation**
Just take the **Phasmatrix.exe** file from the **PROGRAM** folder and place it to your desired location on the computer.



### **Prerequisites**
You need to have an **internet connection established** as well as **Steam running** [in background].



### **How to start**
Start Phasmatrix.exe

The program will start Phasmophobia for you. After 7 and a half seconds in, the on-screen GUI of Phasmatrix will appear on top of Phasmophobia.
When you quit Phasmophobia, Phasmatrix will shut down 1-5 seconds after you returned to your desktop.



### **How to use**
On the left side, you will see all ghosts. Ghosts still left to consider are written in white, ghosts already eliminated will be dimmed to gray.
On the bottom line, you see the current task/suggestion. You can confirm a task to be done, answer yes or no to a question or cycle through all currently remaining tasks/suggestions using "next". The global hotkeys will always be displayed in small red font. Those hotkeys include:

CTRL + 1 --> Positive answer to a task or a question.

CTRL + 2 --> Negative answer to a question.

CTRL + 3 --> Display the next task or question. Only those tasks or questions that still make sense in the current situation will be displayed.

CTRL + M --> Manually eliminate a ghost from the list. After pressing the hotkey, type the name of the ghost exactly as it is written on the left. Press ENTER or RETURN after that to bring focus back to Phasmophobia and eliminate the ghost.

CTRL + R --> Reset Phasmatrix, for example when a new round starts.



## **Side notes**
This program was created by docfo4r

You are allowed to download and use the program. You are also allowed to download and modify the source code in any way. Please leave credit information to docfo4r in and reference to this GitLab page.
This program was written in C# WPF.

Happy hunting!
