﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;

namespace Phasmatrix
{
    static class Starter
    {
        //  Get window handle code taken from here:
        //  https://gist.github.com/GenesisFR/de0ad5710fb5eb221b5a239e819728bf

        private const int SW_SHOWNA = 8;

        //  We need some methods from user32.dll, so lets import them
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern bool EnumWindows(EnumWindowsProc enumProc, IntPtr lParam);
        public delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private extern static int ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        private static extern int SetForegroundWindow(IntPtr hwnd);

        //  We also need a reference to Phasmatrix as well as Phasmophobia, so we can switch between them later on
        static MainWindow main;
        static IntPtr phasmoWindow;

        public static void Start(MainWindow _main)
        {
            main = _main;

            try
            {
                // First we need to know where Steam.exe is
                string steamApp = GetSteam();

                //  Start Steam game: Phasmo
                Process.Start(steamApp, @"steam://rungameid/739630");

                //  Lets wait a bit for Phasmo to load etc.
                Thread.Sleep(7500);

                //  Now find Phasmo window
                phasmoWindow = FindWindowHandle("Phasmophobia");

                //  Place this thing on top of it but don't steal focus from Phasmo
                ShowWindow(phasmoWindow, SW_SHOWNA);

                //  Also check every 5 seconds if Phasmo still runs. If not, close Phasmatrix
                //  There are better ways to do this, like catching events. But that requires admin rights upon execution which I want to avoid
                System.Timers.Timer processCheckTimer = new System.Timers.Timer();
                processCheckTimer.Elapsed += ProcessCheckTimer_Elapsed;
                processCheckTimer.Interval = 5000;
                processCheckTimer.Enabled = true;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Error while starting", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }
        }

        public static void BringFocusBackToPhasmo()
        {
            //  This is called after writing something into the manual input field and hitting ENTER or RETURN
            SetForegroundWindow(phasmoWindow);
            ShowWindow(phasmoWindow, SW_SHOWNA);
        }

        private static void ProcessCheckTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //  Here we check every 5 seconds if Phasmophobia is still running and if not, lets close Phasmatrix aswell
            Process[] proc = Process.GetProcessesByName("Phasmophobia");
            if (proc == null || proc.Length == 0) main.Shut(); 
        }

        static string GetSteam()
        {
            //  We need Steam to be running. If it doesn't run, display message
            Process[] processes = Process.GetProcessesByName("steam");

            if (processes.Length > 0) return processes[0].MainModule.FileName;
            else
            {
                MessageBox.Show("Steam must be running. Please make sure to start Steam first!", "Error while starting", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
                return string.Empty;
            }
        }

        public static IntPtr FindWindowHandle(string processName)
        {
            //  With this piece of magic we are checking for a handle to a certain process by the name of the process
            List<IntPtr> handleList = new List<IntPtr>();
            Process[] processes = Process.GetProcessesByName(processName);
            Process proc = null;

            EnumWindows(delegate (IntPtr hWnd, IntPtr lParam)
            {
                GetWindowThreadProcessId(hWnd, out int processId);

                proc = processes.FirstOrDefault(p => p.Id == processId);

                if (proc != null)
                {
                    handleList.Add(hWnd);
                }

                return true;
            }, IntPtr.Zero);

            return handleList.First();
        }

        public static void ChangeFocusToTextBox()
        {
            //  The user wants to input something to the manual input field. Lets bring Phasmatrix to focus, Phasmophobia will sleep a bit in the background...
            Process thisProcess = Process.GetProcessesByName("Phasmatrix").FirstOrDefault();

            if (thisProcess != null) SetForegroundWindow(thisProcess.MainWindowHandle);
            main.inputBox.Focus();
        }
    }
}