﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Phasmatrix
{
    public partial class MainWindow : Window
    {
        //////////////////////////////////////
        //									//
        //		P H A S M A T R I X			//
        //									//
        //////////////////////////////////////
        //
        //	by docfo4r
        //
        //	Program download link: https://gitlab.com/docfo4r/phasmatrix
        //	Database link: https://gitlab.com/docfo4r/phasmatrix/-/raw/main/SOURCE/phasmatrix.ini
        //
        //
        //___________________________________

        public List<Ghost> ghostList = new List<Ghost>();
        public List<string> firstEvidences = new List<string>();
        public List<Ghost> possibleGhosts = new List<Ghost>();

        StackPanel inputArea;
        public TextBox inputBox;

        public enum HotKeyTypes
        {
            Reset,
            ManualInput,
            Yes,
            No,
            Next
        }

        public enum InvestigationSteps
        {
            Preparation,
            GhostRoom,
            Evidences
        }
        InvestigationSteps investigationStep;

        public enum CheckStates
        {
            yes,
            no,
            notChecked
        }

        public List<PreparationlistEntry> preparationList = new List<PreparationlistEntry>();
        public List<ActionListEntry> actionList = new List<ActionListEntry>();

        public MainWindow()
        {
            InitializeComponent();

            //  At the very first, size window to screen
            this.Loaded += new RoutedEventHandler(Window_Loaded);

            //  Now we check internet and load in our Ghost data
            string dbVersion = Database.ReadDatabase(this);

            //  Now we are starting Phasmo and doing our magic
            Starter.Start(this);

            investigationStep = InvestigationSteps.Preparation;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //  Resize Phasmatrix to screen
            var desktopWorkingArea = SystemParameters.WorkArea;
            Left = desktopWorkingArea.Left;
            Top = desktopWorkingArea.Top;
            Width = desktopWorkingArea.Width;
            Height = desktopWorkingArea.Height;

            //  Add manual entry box to the left stackpanel and hide it
            StackPanel stackPanel = new StackPanel();
            stackPanel.Orientation = Orientation.Horizontal;
            ghostsPanel.Children.Add(stackPanel);
            TextBlock tBlock = new TextBlock();
            tBlock.Text = "Manual elimination:";
            tBlock.FontSize = 25;
            tBlock.Foreground = Brushes.Green;
            stackPanel.Children.Add(tBlock);
            TextBox tBox = new TextBox();
            tBox.FontSize = 25;
            tBox.Background = Brushes.Transparent;
            tBox.BorderThickness = new Thickness(.5);
            tBox.BorderBrush = Brushes.Green;
            tBox.Foreground = Brushes.Lime;
            tBox.MaxLength = 15;
            tBox.CharacterCasing = CharacterCasing.Lower;
            tBox.AcceptsReturn = tBox.AcceptsTab = false;
            stackPanel.Children.Add(tBox);
            inputBox = tBox;
            inputBox.KeyDown += InputBox_KeyDown;
            inputArea = stackPanel;
            inputArea.Visibility = Visibility.Hidden;

            //  Add all ghosts to the left stackpanel
            foreach(Ghost ghost in ghostList)
            {
                TextBlock tb = new TextBlock();
                tb.Name = ghost.name.Replace(" ", "_");
                tb.Text = ghost.name;
                tb.Foreground = Brushes.White;
                tb.FontSize = 20;
                ghostsPanel.Children.Add(tb);
            }

            //  Add info to left stackpanel
            TextBlock tb1 = new TextBlock();
            tb1.Foreground = Brushes.Red;
            tb1.FontSize = 10;
            tb1.Text = "[CTRL + R] reset";
            ghostsPanel.Children.Add(tb1);
            TextBlock tb2 = new TextBlock();
            tb2.Foreground = Brushes.Red;
            tb2.FontSize = 10;
            tb2.Text = "[CTRL + M] manual ghost elimination";
            ghostsPanel.Children.Add(tb2);

            //  All values to default
            Reset();
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            //  When we finished loading, lets hookup the global hotkeys
            base.OnSourceInitialized(e);
            HotKeyManager.RegisterAllHotKeys(this);
        }

        protected override void OnClosed(EventArgs e)
        {
            //  When Phasmatrix gets closed, don't forget to unregister the global hotkeys
            HotKeyManager.UnRegisterAllHotKeys();
            base.OnClosed(e);
        }

        public void Shut()
        {
            //  Phasmo has closed, lets invoke closing Phasmatrix aswell
            Dispatcher.Invoke(new Action(Application.Current.Shutdown));
        }

        public void Reset()
        {
            //  Reset is called at the start of Phasmatrix as well as when hotkey is pressed
            possibleGhosts.Clear();
            possibleGhosts = new List<Ghost>(ghostList);

            investigationStep = InvestigationSteps.Preparation;

            //  Reset preparation/action steps
            foreach(PreparationlistEntry prepEntry in preparationList) prepEntry.done = false;
            foreach (ActionListEntry actionEntry in actionList) actionEntry.state = CheckStates.notChecked;
            currentDisplay = 0;

            //  Reset manual input
            inputBox.Text = string.Empty;
            inputArea.Visibility = Visibility.Hidden;

            //  Update the visuals
            UpdateVisuals();
        }

        void UpdateVisuals()
        {
            //  Change opacity of ghosts on the left depending on what ghosts remain in question
            foreach(UIElement element in ghostsPanel.Children)
            {
                if (element is TextBlock)
                {
                    if ((element as TextBlock).Foreground == Brushes.Red) continue;

                    if (CheckPossibleGhost((element as TextBlock).Text)) (element as TextBlock).Opacity = 1;
                    else (element as TextBlock).Opacity = 0.1;
                }
            }

            //  Change bottom text and hotkey displayment depending on current investigation step
            switch(investigationStep)
            {
                case InvestigationSteps.Preparation:
                    textLine1.Text = preparationList[currentDisplay].task;
                    stackYesNo.Visibility = Visibility.Collapsed;
                    stackYesNoNext.Visibility = Visibility.Collapsed;
                    stackDoneNext.Visibility = Visibility.Visible;
                    break;
                case InvestigationSteps.GhostRoom:
                    textLine1.Text = "Find the ghost room by activity [thrown items, knocks, light switching]. Do you see your breath in there?";
                    stackYesNo.Visibility = Visibility.Visible;
                    stackYesNoNext.Visibility = Visibility.Collapsed;
                    stackDoneNext.Visibility = Visibility.Collapsed;
                    break;
                case InvestigationSteps.Evidences:
                    textLine1.Text = actionList[currentDisplay].task;
                    stackYesNo.Visibility = Visibility.Collapsed;
                    stackYesNoNext.Visibility = Visibility.Visible;
                    stackDoneNext.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        bool CheckPossibleGhost(string textblockText)
        {
            //  Helper method to check if a ghost name is within the possible ghosts list
            bool found = false;

            foreach(Ghost ghost in possibleGhosts)
            {
                if (ghost.name == textblockText)
                {
                    found = true;
                    break;
                }
            }

            return found;
        }

        #region HotKeys
        int currentDisplay;
        public void Next()
        {
            //  Next button was pressed, jump to next logical task depending on the investigation step
            if (investigationStep != InvestigationSteps.GhostRoom)
            {
                if (investigationStep == InvestigationSteps.Preparation)
                {
                    currentDisplay++;
                    if (currentDisplay >= preparationList.Count) currentDisplay = 0;
                    if (preparationList[currentDisplay].done) Next();
                }
                else
                {
                    currentDisplay++;
                    if (currentDisplay >= actionList.Count) currentDisplay = 0;
                    if (actionList[currentDisplay].state != CheckStates.notChecked) Next();
                }

                //  Update the visuals
                UpdateVisuals();
            }
        }

        public void Yes()
        {
            //  Yes was pressed, either count in a ghost or go to the next investigation step
            switch(investigationStep)
            {
                case InvestigationSteps.Preparation:
                    preparationList[currentDisplay].done = true;

                    bool somethingLeft = false;
                    foreach(PreparationlistEntry entry in preparationList)
                    {
                        if (!entry.done)
                        {
                            somethingLeft = true;
                            break;
                        }
                    }

                    if (!somethingLeft) investigationStep = InvestigationSteps.GhostRoom;
                    else Next();
                    UpdateVisuals();
                    break;
                case InvestigationSteps.GhostRoom:
                    SwitchToEvidenceHunt(true);
                    break;
                case InvestigationSteps.Evidences:
                    actionList[currentDisplay].state = CheckStates.yes;
                    EliminateGhostsFromSecondEvidence();
                    UpdateActionList();
                    Next();
                    UpdateVisuals();
                    break;
            }
        }

        public void No()
        {
            //  No was pressed, remove freezing evidence from possibility [investigation step 1] or current ghost test [investigation step 2]
            switch(investigationStep)
            {
                case InvestigationSteps.GhostRoom:
                    SwitchToEvidenceHunt(false);
                    break;
                case InvestigationSteps.Evidences:
                    actionList[currentDisplay].state = CheckStates.no;
                    EliminateGhostsFromSecondEvidence();
                    UpdateActionList();
                    Next();
                    break;
            }
        }

        public void ManualInput()
        {
            //  Manual input is desired, show and set focus to manual input field
            if (inputArea.Visibility == Visibility.Visible) return;
            inputArea.Visibility = Visibility.Visible;

            inputBox.Text = string.Empty;
            Starter.ChangeFocusToTextBox();
        }

        private void InputBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            //  A button was pressed on the input field.
            //  If that certain button was ENTER or RETURN, close the input field, bring focus back to Phasmo and eliminate ghost if there is a match
            if (e.Key == System.Windows.Input.Key.Return || e.Key == System.Windows.Input.Key.Enter)
            {
                inputArea.Visibility = Visibility.Hidden;

                foreach(Ghost ghost in possibleGhosts)
                {
                    if (ghost.name.ToLower() == inputBox.Text.ToLower())
                    {
                        possibleGhosts.Remove(ghost);
                        EliminateGhostsFromSecondEvidence();
                        UpdateActionList();
                        Next();
                        break;
                    }
                }

                Starter.BringFocusBackToPhasmo();
                e.Handled = true;
            }
        }

        void SwitchToEvidenceHunt(bool freezing)
        {
            //  Ghost room was found and player tells us if it's freezing or not
            //  Depending on this choice, activate freezing or non-freezing ghosts and progress the investigation step
            possibleGhosts = new List<Ghost>(ghostList);

            foreach(Ghost ghost in ghostList)
            {
                if (freezing && !ghost.evidences.Contains("Freezing temperatures")) possibleGhosts.Remove(ghost);
                else if (!freezing && ghost.evidences.Contains("Freezing temperatures")) possibleGhosts.Remove(ghost);
            }

            investigationStep = InvestigationSteps.Evidences;
            currentDisplay = 0;

            UpdateActionList();
            Next();
            UpdateVisuals();
        }

        void EliminateGhostsFromSecondEvidence()
        {
            //  A ghost is not in question any more, remove that ghost from the possible ghosts list and update visuals
            //  This can happen when the player jumped from investigation step 1to 2 [after finding the ghost room], from a resolved check or manual input
            foreach(Ghost ghost in ghostList)
            {
                foreach(ActionListEntry entry in actionList)
                {
                    if (entry.task.StartsWith(ghost.name))
                    {
                        if (entry.state == CheckStates.no && possibleGhosts.Contains(ghost)) possibleGhosts.Remove(ghost);
                        else if (!possibleGhosts.Contains(ghost) && entry.state == CheckStates.yes) possibleGhosts.Add(ghost);
                    }
                    else
                    {
                        if (entry.state == CheckStates.no)
                        {
                            foreach(string ghostToEliminate in entry.affectedGhostList)
                            {
                                possibleGhosts.RemoveAll(g => g.name == ghostToEliminate);
                            }
                        }
                    }
                }
            }

            //  After that ghost has been removed, updated the visuals
            UpdateVisuals();
        }

        void UpdateActionList()
        {
            //  If a ghost was eliminated, check all actions in the action list if there are tasks for the ghost that has been eliminated. These tasks can go now!
            foreach(Ghost ghost in ghostList)
            {
                if (!possibleGhosts.Contains(ghost))
                {
                    foreach(ActionListEntry entry in actionList)
                    {
                        if (entry.task.StartsWith(ghost.name)) entry.state = CheckStates.no;
                    }
                }
            }
        }
        #endregion
    }

    public class PreparationlistEntry
    {
        public bool done;
        public string task;
    }

    public class ActionListEntry
    {
        public MainWindow.CheckStates state;
        public string task;
        public List<string> affectedGhostList = new List<string>();
    }
}