﻿using System;
using System.Linq;
using System.Net;
using System.Windows;

namespace Phasmatrix
{
    static class Database
    {
        static string databaseLink = @"https://gitlab.com/docfo4r/phasmatrix/-/raw/main/SOURCE/phasmatrix.ini";
        static string databaseVersion = string.Empty;

        public static string ReadDatabase(MainWindow main)
        {
            //  Let's get some juicy ghost data
            bool databaseReady = DownloadData(main);

            //  Something went wrong while downloading data. As we have no data, there is no point in continuing from here
            //  ...but we are friendly and the let user know that there was a problem with the download.
            if (!databaseReady)
            {
                MessageBoxResult mbResult = MessageBox.Show("Cannot read database. Please check your internet." + Environment.NewLine + Environment.NewLine +
                    "Would you like to try again?", "Network error", MessageBoxButton.YesNo, MessageBoxImage.Warning);

                if (mbResult == MessageBoxResult.No) Application.Current.Shutdown();
                else ReadDatabase(main);
            }

            return databaseVersion;
        }

        static bool DownloadData(MainWindow main)
        {
            bool result = false;

            try
            {
                //  The data resides in a public GitLab ini file within the Phasmatrix project.
                //  We need a webclient to download the content of this file.
                //  We separate each line nicely into an array of lines.
                WebClient client = new WebClient();
                string content = client.DownloadString(databaseLink);
                string[] contentByLines = content.Split('\n');

                //  Now that we got the array, lets loop through the content
                foreach(string line in contentByLines)
                {
                    //  Do not read empty or commented lines
                    if (line == string.Empty || line.StartsWith(@"//")) continue;

                    //  Get version of database file
                    if (line.ToLower().StartsWith("version")) databaseVersion = line.Split('=')[1];

                    //  Get all preparation tasks
                    if (line.ToLower().StartsWith("preparation"))
                    {
                        PreparationlistEntry prepEntry = new PreparationlistEntry();
                        prepEntry.done = false;
                        prepEntry.task = line.Split('=')[1];
                        main.preparationList.Add(prepEntry);
                    }

                    //  Get all ghosts
                    if (line.ToLower().StartsWith("ghosts"))
                    {
                        string[] ghosts = line.Split('=')[1].Split(';');

                        foreach (string ghostEntry in ghosts)
                        {
                            Ghost ghost = new Ghost();
                            ghost.name = ghostEntry;
                            main.ghostList.Add(ghost);
                        }
                    }

                    //  Get all ghost evidences
                    var match = main.ghostList.FirstOrDefault(x => x.name == line.Split('=')[0]);
                    if (match != null && match.evidences[0] == null)
                    {
                        match.evidences[0] = line.Split('=')[1].Split(';')[0];
                        match.evidences[1] = line.Split(';')[1];
                        match.evidences[2] = line.Split(';')[2];

                        if (!main.firstEvidences.Contains(line.Split('=')[1].Split(';')[0]))
                            main.firstEvidences.Add(line.Split('=')[1].Split(';')[0]);
                        if (!main.firstEvidences.Contains(line.Split(';')[1])) main.firstEvidences.Add(line.Split(';')[1]);
                        if (!main.firstEvidences.Contains(line.Split(';')[2])) main.firstEvidences.Add(line.Split(';')[2]);
                    }

                    //  Get general test tests
                    if (line.ToLower().StartsWith("generaltest"))
                    {
                        ActionListEntry actionListEntry = new ActionListEntry();
                        actionListEntry.state = MainWindow.CheckStates.notChecked;
                        actionListEntry.task = line.Split('=')[1] + " test: " + line.Split('=')[3];
                        actionListEntry.affectedGhostList = line.Split('=')[2].Split(';').ToList();
                        main.actionList.Add(actionListEntry);
                    }

                    //  Get all unique ghost descriptions
                    if (match != null && line != match.name + "=" + match.evidences[0] + ";" + match.evidences[1] + ";" + match.evidences[2])
                    {
                        match.descriptions.Add(line.Split('=')[1]);

                        foreach (string secondEvidence in match.descriptions)
                        {
                            ActionListEntry actionListEntry = new ActionListEntry();
                            actionListEntry.state = MainWindow.CheckStates.notChecked;
                            actionListEntry.task = match.name + " test: " + secondEvidence;
                            main.actionList.Add(actionListEntry);
                        }
                    }
                }

                result = true;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Error reading database", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return result;
        }
    }
}