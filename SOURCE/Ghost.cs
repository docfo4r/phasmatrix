﻿using System.Collections.Generic;

namespace Phasmatrix
{
    public class Ghost
    {
        public string name = string.Empty;
        public string[] evidences = new string[3];
        public List<string> descriptions = new List<string>();
    }
}