﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Interop;

namespace Phasmatrix
{
    static class HotKeyManager
    {
        //  Lets first declare some hotkey IDs
        private const int HOTKEY_ID_RESET = 9000;
        private const int HOTKEY_ID_MANUAL = 9001;
        private const int HOTKEY_ID_YES = 9002;
        private const int HOTKEY_ID_NO = 9003;
        private const int HOTKEY_ID_NEXT = 9004;

        //  We need to keep track of windows
        static HwndSource _source;
        static MainWindow main;

        //  We also need to use user32.dll which holds the methods to register and unregister global hotkeys
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        //  A key was pressed
        static IntPtr HwndHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            const int WM_HOTKEY = 0x0312;

            //  Lets first check if it's a global hotkey and if so, check which one it was and what to do next
            if (msg == WM_HOTKEY)
            {
                switch (wParam.ToInt32())
                {
                    case HOTKEY_ID_RESET:
                        main.Reset();
                        handled = true;
                        break;
                    case HOTKEY_ID_MANUAL:
                        main.ManualInput();
                        handled = true;
                        break;
                    case HOTKEY_ID_YES:
                        main.Yes();
                        handled = true;
                        break;
                    case HOTKEY_ID_NO:
                        main.No();
                        handled = true;
                        break;
                    case HOTKEY_ID_NEXT:
                        main.Next();
                        handled = true;
                        break;
                }
            }
            return IntPtr.Zero;
        }

        public static void RegisterAllHotKeys(MainWindow _main)
        {
            //  Helper method to register all global hotkeys at the start of the program
            main = _main;
            var helper = new WindowInteropHelper(main);
            _source = HwndSource.FromHwnd(helper.Handle);
            _source.AddHook(HwndHook);
            Register();
        }

        public static void UnRegisterAllHotKeys()
        {
            //  When Phasmatrix is closed, we need to tell Windows that our Phasmatrix hotkeys must go aswell
            _source.RemoveHook(HwndHook);
            _source = null;
            Unregister();
        }

        static void Register()
        {
            //  We call the registration of each hotkey one by another. They all use CTRL as mod
            var helper = new WindowInteropHelper(main);
            const int MOD_CTRL = 0x0002;

            //  Register reset [CTRL + R]
            int VK = 0x52;
            if (!RegisterHotKey(helper.Handle, HOTKEY_ID_RESET, MOD_CTRL, VK))
            {
                //  Handle error
            }

            //  Register manual input [CTRL + M]
            VK = 0x4D;
            if (!RegisterHotKey(helper.Handle, HOTKEY_ID_MANUAL, MOD_CTRL, VK))
            {
                //  Handle error
            }

            //  Register yes [CTRL + 1]
            VK = 0x61;
            if (!RegisterHotKey(helper.Handle, HOTKEY_ID_YES, MOD_CTRL, VK))
            {
                //  Handle error
            }

            //  Register no [CTRL + 2]
            VK = 0x62;
            if (!RegisterHotKey(helper.Handle, HOTKEY_ID_NO, MOD_CTRL, VK))
            {
                //  Handle error
            }

            //  Register next [CTRL + 3]
            VK = 0x63;
            if (!RegisterHotKey(helper.Handle, HOTKEY_ID_NEXT, MOD_CTRL, VK))
            {
                //  Handle error
            }
        }

        static void Unregister()
        {
            //  Unregister all Phasmatrix hotkeys one by another
            var helper = new WindowInteropHelper(main);
            UnregisterHotKey(helper.Handle, HOTKEY_ID_RESET);
            UnregisterHotKey(helper.Handle, HOTKEY_ID_MANUAL);
            UnregisterHotKey(helper.Handle, HOTKEY_ID_YES);
            UnregisterHotKey(helper.Handle, HOTKEY_ID_NO);
            UnregisterHotKey(helper.Handle, HOTKEY_ID_NEXT);
        }
    }
}